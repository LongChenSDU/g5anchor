# g5anchor

"g5anchor" is a procedure, written in Mathematica, to anchor gamma5-matrix in a Dirac trace in Dimensional Regularization in Standard-Model Feynman diagrams.
For the description of the underlying idea, please refer to arXiv:2409.08099.


# Installation

The entire package is contained in the Mathematica script g5anchor.m, which can be loaded using the Get[] function in a Mathematica program to have access to the functions defined therein. There are no dependencies on external packages other than the built-in functions of Mathematica itself.

The latest version can be downloaded from https://gitlab.com/LongChenSDU/g5anchor


# Usage 

For a quick overview of what the package g5anchor is used for and how to use the functions defined therein, one may start with the output messages from ?AnchorG5 (the master function in this package), ?Max1PIopenVFFsIOlegs and ?ManipulateG5inChain after loading the package in a Mathematica session.

Several examples are provided in `examples` folder.


# Support 

The current implementation is only partly checked using very limited examples available to the author so far.
If you have any questions or advice concerning this package, please do not hesitate to write to longchen@sdu.edu.cn


# Statement

This program is free software and is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY. 
You can redistribute it and/or modify it, and if the functionalities it offers are helpful to your research, it would be very kind of you to add references to the program and the associated articles in your work.
